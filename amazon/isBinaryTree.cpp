#include <iostream>
#include <cstdlib>
#include <limits.h>

using namespace std;

typedef struct TreeNode{
    TreeNode(int val):data(val),left(NULL),right(NULL){}

    int data;
    struct TreeNode *left, *right;

}TreeNode;


TreeNode *MK(int data){
   TreeNode *newNode = new TreeNode(data);

   return newNode;
}


bool isBSTUtil(TreeNode *tree, int min, int max){
   if( tree == NULL ){
       return true;
   }
   if(tree->data < min || tree->data > max){
       return false;
   }
   return isBSTUtil(tree->left, min, tree->data-1)
       && isBSTUtil(tree->right, tree->data+1, max);
}

// 1. left subtree contains only nodes with keys < the node's key
// 2. right subtree contains only nodes with keys > the node's key
// 3. both left and right subtrees must also be binary search trees
bool isBST(TreeNode *node){
   return isBSTUtil(node, INT_MIN, INT_MAX);
}

int main(void){
    TreeNode *root = MK(4);
    root->left = MK(2);
    root->right = MK(5);
    root->left->left = MK(1);
    root->left->right = MK(3);
  
    if(isBST(root))
        cout << "Is BST\n";
    else
        cout << "Is not a BST\n";

    return 0;
    return 0;
}
