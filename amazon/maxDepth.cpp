#include <iostream>
#include <cstdlib>
#include <limits.h>

using namespace std;

typedef struct TreeNode{
    TreeNode(int val):data(val),left(NULL),right(NULL){}

    int data;
    struct TreeNode *left, *right;

}TreeNode;


TreeNode *MK(int data){
   TreeNode *newNode = new TreeNode(data);

   return newNode;
}

int maxDepth(TreeNode *tree){

    if(tree == NULL) return 0;

    int lDepth = maxDepth(tree->left);
    int rDepth = maxDepth(tree->right);

    return max(lDepth,rDepth) + 1;
}

int main(void){
    TreeNode *root = MK(4);
    root->left = MK(2);
    root->right = MK(5);
    root->left->left = MK(1);
    root->left->right = MK(3);
  
    cout << "Height of tree is " << maxDepth(root) << endl;
    return 0;
}
