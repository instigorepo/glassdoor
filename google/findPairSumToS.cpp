#include <iostream>
#include <vector>
#include <list>

using namespace std;


int bsearch(vector<int> list, int search, int lo, int hi){
    
    while(lo <= hi){
        int mid = lo + (hi-lo)/2;
        if(list[mid] == search){
            return mid;
        }
        else if(search > list[mid]){
            lo = mid + 1;
        }
        else{
            hi = mid - 1;
        }
    }
    return -1; // not found
} 

void findPairSum(vector<int> input, int S){

    int N = input.size()-1;
    for(int i = 0; i < N; ++i){
        if(bsearch(input, S-input[i], 0, N)){
            cout << "(" << input[i] << "," << S-input[i] << ")" << endl;    
        }
    }

}

int main(void){
    
    pair <int,int> P;
    vector<int> V;
    int S = 20;
    
    V.push_back(5);
    V.push_back(15);
    V.push_back(14);
    V.push_back(6);
    V.push_back(7);
    V.push_back(10);
    V.push_back(20);
    V.push_back(13);
    V.push_back(9);
    findPairSum(V,S);

    return 0; 
}
