#include <iostream>
#include <stack>

using namespace std;


bool parens(string input){

    stack<char> S;
    int N = input.size();

    for(int i = 0; i < N; ++i){
        if(input[i] == '{'){
            S.push('{');
        }
        else{
            S.pop();
        }
    }
    return S.empty();   
}

int main(void){
    
    string str("{{{{{}}}}}");
    if(parens(str)){
        cout << "found matching brackets\n"; 
    }
    else{
        cout << "found brackets that do not match\n";
    }
    return 0;
}
